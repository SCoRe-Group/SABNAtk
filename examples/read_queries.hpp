#ifndef READ_QUERIES_HPP
#define READ_QUERIES_HPP

#include <fstream>
#include <vector>

#include "jaz/iterator.hpp"
#include "jaz/string.hpp"

#include <bit_util.hpp>


template <typename SetType>
inline bool read_queries(const std::string& name, std::vector<std::pair<SetType, SetType>>& Q) {
    std::ifstream f(name);
    if (!f) return false;

    jaz::getline_iterator<> iter(f), end;
    std::vector<std::string> tokens;

    std::istringstream iss;
    int x;

    for (; iter != end; ++iter) {
        tokens.clear();
        jaz::split('|', *iter, std::back_inserter(tokens));

        SetType xi = set_empty<SetType>();

        iss.clear();
        iss.str(tokens[2]);
        while (iss >> x) xi = set_add(xi, x);

        SetType pa = set_empty<SetType>();

        iss.clear();
        iss.str(tokens[1]);
        while (iss >> x) pa = set_add(pa, x);

        Q.emplace_back(xi, pa);
    } // for iter

    return true;
} // read_queries

#endif // READ_QUERIES_HPP

#include <iostream>
#include <sabnatk.hpp>


struct call {
    // called by query engine before processing the stream
    // engine passes ri, i.e. the number of states of Xi,
    // and qi, the number of states parents of Xi MAY assume
    void init(int ri, int qi) {
        std::cout << "init, ri=" << ri << ", qi=" << qi << std::endl;
        count = 0;
    } // init

    // called by query engine after processing of the stream is done
    // engine passes qi, the ACTUAL number of states
    // that parents of Xi assumed
    void finalize(int qi) {
        std::cout << "finalize, qi=" << qi << std::endl;
    } // finalize

    void operator()(int Nij) {
        std::cout << "call from CQE with Nij=" << Nij << std::endl;
    } // operator()

    void operator()(int Nijk, int Nij) {
        std::cout << "call from CQE with Nijk=" << Nijk << std::endl;
        count++;
    } // operator()

    // access to internal state (return value is specified by user)
    int score() const { return count; }

    // user specified internal state
    int count = 0;
}; // struct call


int main(int argc, char* argv[]) {
    // 4 variables (say X0, X1, X2, X3), 8 observations
    std::vector<uint8_t> D{ 0, 0, 0, 0, 1, 1, 1, 1, \
                            1, 0, 0, 0, 0, 0, 0, 1, \
                            1, 1, 1, 1, 1, 1, 1, 0, \
                            1, 1, 1, 1, 2, 2, 0, 0};

    // use one word (64bit) because n < 64
    auto count = sabnatk::create_counter<1, sabnatk::CT>(4, 8, std::begin(D));
    using set_type = decltype(count)::set_type;

    auto xi = set_empty<set_type>();
    auto pa = set_empty<set_type>();

    // let's count X0=0 and [X1=0,X2=1]:

    // first variables
    xi = set_add(xi, 0);
    pa = set_add(pa, 1);
    pa = set_add(pa, 2);

    // then states
    std::vector<uint8_t> sxi{0};
    std::vector<uint8_t> spa{1, 1};

    // callback for each xi
    std::vector<call> C(set_size(xi));

    // and here we go!
    count.apply(xi, pa, sxi, spa, C);
    std::cout << C[0].score() << std::endl;

    // now let's do full counting query for two variables
    // i.e., we get counts for (X0|X1,X2) and for (X3|X1,X2)
    xi = set_add(xi, 3);
    C.resize(set_size(xi));

    count.apply(xi, pa, C);

    std::cout << C[0].score() << " " << C[1].score() << std::endl;

    return 0;
} // main

# we use bitarray to represent sets
from bitarray import bitarray
from sabnatk.BVCounter import MDL64

import csv


if __name__ == "__main__":
    # read input data (in SABNA csv format)
    dat = "data/alarm.csv"

    n = 0
    m = 0
    D = []

    with open(dat, "rt") as cf:
        rd = csv.reader(cf, delimiter = ' ')
        for row in rd:
            n = n + 1
            m = len(row)
            D = D + list(map(int, row))

    # create scoring function with support for <=64 variables
    mdl = MDL64()
    mdl.init(n, m, D)

    # create set of Xi variables
    # endian must be little
    xi = bitarray(37, endian = "little")
    xi.setall(0)
    xi[0] = 1
    xi[7] = 1
    xi[9] = 1

    # create set of Pa(Xi) that is shared by all Xi
    pa = bitarray(37, endian = "little")
    pa.setall(0)
    pa[3] = 1
    pa[5] = 1
    pa[8] = 1
    pa[29] = 1
    pa[30] = 1
    pa[31] = 1

    # compute MDL(Xi|Pa(Xi)) for all Xi
    # internally we use memoryview to eliminate extra memory copies
    S = mdl.score(xi.tobytes(), pa.tobytes())
    print("test:", S)

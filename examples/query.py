from sabnatk.BVCounter import Query256
import csv

if __name__ == "__main__":
    dat = "data/alarm.csv"

    n = 0
    m = 0
    D = []

    with open(dat, "rt") as cf:
        rd = csv.reader(cf, delimiter = ' ')
        for row in rd:
            n = n + 1
            m = len(row)
            D = D + list(map(int, row))

    q = Query256()
    q.init(n, m, D)

    L = q.run([0], [3, 5], [1], [0, 0])
    print(L)

    L = q.run([0, 2], [3, 5], [1, 0], [0, 0])
    print(L)

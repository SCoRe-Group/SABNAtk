#include <chrono>
#include <iostream>
#include <vector>

#include <bit_util.hpp>
#include <sabnatk.hpp>

#include "csv.hpp"
#include "read_queries.hpp"


class estimator {
public:
    void init(int ri, int qi) { state_ = 0.0; }

    void finalize(int qi) { }

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) { state_ = static_cast<double>(Nijk) / Nij; }

    double state() const { return state_; }

private:
    double state_ = 0.0;

}; // class estimator


int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "usage: random_query csv_file dat_file" << std::endl;
        return -1;
    }

    // GET INPUT
    std::cout << "reading input..." << std::endl;

    const int N = 2;

    using set_type = uint_type<N>;
    using data_type = uint8_t;

    std::vector<data_type> D;

    auto [res, n, m] = read_csv(argv[1], D);

    if (res == false) {
        std::cout << "error: unable to read input data" << std::endl;
        return -1;
    }

    std::cerr << "n=" << n << ", m=" << m << std::endl;


    std::vector<std::pair<set_type, set_type>> Q;

    if (!read_queries(argv[2], Q)) {
        std::cout << "error: unable to read queries" << std::endl;
        return -1;
    }

    std::cerr << "qs=" << Q.size() << std::endl;


    // CREATE AUTO COUNTER
    std::cout << "building engine..." << std::endl;

    auto eng = sabnatk::create_counter<N, sabnatk::Auto>(n, m, std::begin(D));


    // RUN TEST
    std::cout << "running queries..." << std::endl;

    std::vector<estimator> F(1);

    auto t0 = std::chrono::steady_clock::now();
    for (const auto& q : Q) eng.apply(q.first, q.second, F);
    auto t1 = std::chrono::steady_clock::now();

    std::cout << "total time = " << (static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count())) << std::endl;

    return 0;
} // main

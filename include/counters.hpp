/***
 *  $Id$
 **
 *  File: counters.hpp
 *  Created: Jan 18, 2022
 *
 *  Author: Pawel Bratek <pawel.bratek@pcz.pl>
 *  Copyright (c) 2022 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef COUNTERS_HPP
#define COUNTERS_HPP

namespace sabnatk {

  enum counter_type { Rad = 0, BV = 1, CT = 2, Auto = 3 };

} // namespace sabnatk

#endif // COUNTERS_HPP

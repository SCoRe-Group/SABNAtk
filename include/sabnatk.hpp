/***
 *  $Id$
 **
 *  File: sabnatk.hpp
 *  Created: May 20, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2016-2022 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef SABNATK_HPP
#define SABNATK_HPP

#include "AutoCounter.hpp"
#include "BVCounter.hpp"
#include "CTCounter.hpp"
#include "RadCounter.hpp"

#include "counters.hpp"


namespace sabnatk {

  template <int N, counter_type Type, typename Iter>
  auto create_counter(int n, int m, Iter iter, int burn_in = -1, int CT_memory_limit = 32, int required_instances = 3) {
      if constexpr (Type == BV) return create_BVCounter<N>(n, m, iter);
      if constexpr (Type == CT) return create_CTCounter<N>(n, m, iter);
      if constexpr (Type == Rad) return create_RadCounter<N>(n, m, iter);
      if constexpr (Type == Auto) return create_AutoCounter<N>(n, m, iter, burn_in, CT_memory_limit, required_instances);
  } // create_counter

} // namespace sabnatk

#endif // SABNATK_HPP

/***
 *  $Id$
 **
 *  File: PAPIService.hpp
 *  Created: Jan 18, 2022
 *
 *  Author: Pawel Bratek <pawel.bratek@pcz.pl>
 *  Copyright (c) 2022 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef PAPI_SERVICE_HPP
#define PAPI_SERVICE_HPP

#include <stdexcept>
#include <string>

#include <papi.h>


class PAPIService {
public:
    int eventset;
    long long int count;

    PAPIService() {
        int rv = PAPI_library_init(PAPI_VER_CURRENT);

        if ((rv != PAPI_VER_CURRENT) && (rv > 0)) {
            throw std::runtime_error("PAPI failed to initialize: " + std::to_string(rv));
        }

        if (rv < 0) {
            throw std::runtime_error("PAPI init error: " + std::to_string(rv));
        }

        eventset = PAPI_NULL;
        rv = PAPI_create_eventset(&eventset);

        if (rv != PAPI_OK) throw std::runtime_error("PAPI failed to create eventset");

        PAPI_add_named_event(eventset, "PAPI_TOT_CYC");
        PAPI_start(eventset);

        count = 0;
    } // Papi

    ~PAPIService() { PAPI_stop(eventset, &count); }

}; // class PAPIService

#endif // PAPI_SERVICE_HPP

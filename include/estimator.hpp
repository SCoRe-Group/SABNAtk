/***
 *  $Id$
 **
 *  File: AIC.hpp
 *  Created: Mar 31, 2022
 *
 *  Author: Pawel Bratek <pawel.bratek@pcz.pl>
 *  Copyright (c) 2022 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef ESTIMATOR_HPP
#define ESTIMATOR_HPP

class estimator {
public:
    void init(int ri, int qi) { state_ = 0.0; }

    void finalize(int qi) { }

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) { state_ = static_cast<double>(Nijk) / Nij; }

    double state() const { return state_; }

private:
    double state_ = 0.0;

}; // class estimator

#endif // ESTIMATOR_HPP

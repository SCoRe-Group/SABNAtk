/***
 *  $Id$
 **
 *  File: LRmodel.hpp
 *  Created: Jan 18, 2022
 *
 *  Author: Pawel Bratek <pawel.bratek@pcz.pl>
 *  Copyright (c) 2022 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef LR_MODEL_HPP
#define LR_MODEL_HPP

#include <cmath>
#include "counters.hpp"


namespace sabnatk {

  namespace detail {

    class LRmodel {
    public:
        explicit LRmodel(counter_type ct = Rad, bool il = true)
            : counter_(ct), is_linear_(il) { }

        int n() const { return n_; }

        counter_type counter() const { return counter_; }

        double a() const { return cov_xy_ / var_x_; }

        double b() const { return mean_y_ - a() * mean_x_; }

        double evaluate(int x, double K) const {
            if (is_linear_) return a() * x + b();
            return std::pow(K, b()) * std::pow(K, (a() * x));
        } // evaluate

        void update(int x, long long int y, double K) {
            n_++;

            double dx = x - mean_x_;

            var_x_ += ((dx * dx * (n_ - 1) / n_) - var_x_) / n_;
            mean_x_ += dx / n_;

            double dy = is_linear_ ? y : std::log(y) / std::log(K);

            dy -= mean_y_;
            cov_xy_ += ((dx * dy * (n_ - 1) / n_) - cov_xy_) / n_;
            mean_y_ += dy / n_;
        } // update


    private:
        counter_type counter_;

        bool is_linear_;

        int n_ = 0;

        double mean_x_ = 0.0;
        double var_x_ = 0.0;
        double mean_y_ = 0.0;
        double cov_xy_ = 0.0;

    }; // class LRmodel

  } // namespace detail

} // namespace sabnatk

#endif // LR_MODEL_HPP

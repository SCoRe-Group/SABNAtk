/***
 *  $Id$
 **
 *  File: BDeu.hpp
 *  Created: Nov 22, 2016
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2016-2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef BDEU_HPP
#define BDEU_HPP

#include <cmath>
#include <tuple>


class BDeu {
public:
    typedef std::tuple<double, double, double> score_type;

    explicit BDeu(double alpha = 1.0) : alpha_(alpha) { }

    // we are using ln instead of log2!!!
    void init(int ri, int qi) {
        ri_ = ri;
        score_ = 0.0;
        aq_ = alpha_ / qi;
        lnaq_ = std::lgamma(aq_);
        arq_ = alpha_ / (ri * qi);
        lnarq_ = std::lgamma(arq_);
    } // init

    void finalize(int qi) { }

    void operator()(int Nij) {
        score_ += (lnaq_ - std::lgamma(Nij + aq_));
    } // operator

    void operator()(int Nijk, int Nij) {
        score_ += (std::lgamma(Nijk + arq_) - lnarq_);
        ++Kij_;
    } // operator()

    // we return -score because we are considering minimization problem
    score_type score() const { return {-score_, aq_, Kij_ * std::log(ri_)}; }

private:
    double score_ = 0.0;

    double alpha_ = 1.0;

    double aq_ = 1.0;
    double arq_ = 1.0;
    double lnaq_ = 1.0;
    double lnarq_ = 1.0;

    int Kij_ = 0;
    int ri_ = -1;
}; // class BDeu

#endif // BDEU_HPP

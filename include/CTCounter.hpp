/***
 *  $Id$
 **
 *  File: CTCounter.hpp
 *  Created: Nov 04, 2020
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2020-2022 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef CT_COUNTER_HPP
#define CT_COUNTER_HPP

#include <algorithm>
#include <vector>

#include <bit_util.hpp>

#include "counters.hpp"


namespace sabnatk {

  template <int N, typename Data = uint8_t> class CTCounter {
  public:
      using set_type = uint_type<N>;
      using data_type = Data;


      counter_type type() const { return CT; }


      int n() const { return n_; }

      int m() const { return m_; }

      int r(int xi) const { return r_[xi]; }


      template <typename score_functor>
      void apply(const set_type& set_xi, const set_type& pa,
                 const std::vector<data_type>& state_xi,
                 const std::vector<data_type>& state_pa,
                 std::vector<score_functor>& F) {

          std::vector<int> xi_vect = as_vector(set_xi);
          std::vector<int> pa_vect = as_vector(pa);

          int nxi = xi_vect.size();
          int npa = pa_vect.size();

          // handle no parents
          if (npa == 0) {
              for (int i = 0; i < nxi; ++i) {
                  F[i].init(r_[xi_vect[i]], 1);
                  F[i](m_);

                  int Nijk = 0;
                  const data_type* d = &D_[xi_vect[i] * m_];

                  for (int j = 0; j < m_; ++j) if (d[j] == state_xi[i]) Nijk++;

                  F[i](Nijk, m_);
                  F[i].finalize(1);
              }

              return;
          } // if npa

          // generate ids for input states to count
          std::vector<int> codes(nxi, state_pa[0]);

          int qi = 0;

          for (int i = 0; i < nxi; ++i) {
              F[i].init(r_[xi_vect[i]], 1);
              qi = r_[pa_vect[0]];
              for (int z = 1; z < npa; ++z) {
                  codes[i] += (state_pa[z] * qi);
                  qi *= r_[pa_vect[z]];
              }
          }

          // generate ids from observations
          std::vector<int> pa_idx(m_);

          std::copy_n(std::begin(D_) + (pa_vect[0] * m_), m_, std::begin(pa_idx));
          qi = r_[pa_vect[0]];

          for (int z = 1; z < npa; ++z) {
              const data_type* d = &D_[pa_vect[z] * m_];
              for (int j = 0; j < m_; ++j) pa_idx[j] += (qi * d[j]);
              qi *= r_[pa_vect[z]];
          }

          // extract and count matching instances
          std::vector<int> loc;

          for (int i = 0; i < nxi; ++i) {
              loc.clear();
              for (int j = 0; j < m_; ++j) if (pa_idx[j] == codes[i]) loc.push_back(j);
              F[i](loc.size());
              int Nijk = 0;
              for (auto pos : loc) if (D_[xi_vect[i] * m_ + pos] == state_xi[i]) Nijk++;
              F[i](Nijk, loc.size());
          }
      } // apply

      template <typename score_functor>
      void apply(const set_type& set_xi, const set_type& pa, std::vector<score_functor>& F) {
          std::vector<int> xi_vect = as_vector(set_xi);
          std::vector<int> pa_vect = as_vector(pa);
          apply(xi_vect, pa_vect, F);
      } // apply

      template <typename score_functor>
      void apply(const set_type& xi, const set_type& pa, std::vector<score_functor>& F) const {
          std::vector<int> xi_vect = as_vector(xi);
          apply(xi_vect, pa, F);
      } // apply

      template <typename score_functor>
      void apply(const std::vector<int>& xi_vect, const std::vector<int>& pa_vect, std::vector<score_functor>& F) const {
          int nxi = xi_vect.size();

          // handle no parents
          if (pa_vect.empty()) {
              std::vector<int> Nijk;

              for (int i = 0; i < nxi; ++i) {
                  F[i].init(r_[xi_vect[i]], 1);
                  F[i](m_);

                  Nijk.resize(r_[xi_vect[i]]);
                  std::fill(std::begin(Nijk), std::end(Nijk), 0);

                  const data_type* d = &D_[xi_vect[i] * m_];
                  for (int j = 0; j < m_; ++j) Nijk[d[j]]++;

                  for (auto nijk : Nijk) if (nijk > 0) F[i](nijk, m_);

                  F[i].finalize(1);
              } // for i

              return;
          } // if pa_vect


          std::vector<int> pa_idx(m_);

          auto iter = std::begin(pa_vect);
          auto end = std::end(pa_vect);

          // index all observed configurations of parents
          std::copy_n(std::begin(D_) + (*iter * m_), m_, std::begin(pa_idx));
          long long int qi = r_[*iter];

          for (++iter; iter != end; ++iter) {
              const data_type* d = &D_[*iter * m_];
              for (int j = 0; j < m_; ++j) pa_idx[j] += (qi * d[j]);
              qi *= r_[*iter];
          }

          // prepare memory for CT
          int rri = r_[xi_vect[0]];
          std::vector<int> r_idx(nxi, 0);

          for (int i = 1; i < nxi; ++i) {
              r_idx[i] = r_idx[i -1] + (r_[xi_vect[i]] * qi);
              rri *= r_[xi_vect[i]];
          }

          std::vector<int> ct(rri * qi, 0);

          // populate CT
          for (int i = 0; i < nxi; ++i) {
              int xi = xi_vect[i];
              int pos = r_idx[i];

              const data_type* d = &D_[xi * m_];

              for (int j = 0; j < m_; ++j) {
                  long long int loc = pos + (d[j] * qi) + pa_idx[j];
                  ct[loc]++;
              }
          }

          // emit counts to the user
          int Nij = 0;
          int Nijk = 0;

          for (int i = 0; i < nxi; ++i) {
              int ri = r_[xi_vect[i]];

              F[i].init(ri, qi);

              int pos = r_idx[i];

              for (int j = 0; j < qi; ++j) {
                  int Nij = 0;
                  for (int k = 0; k < ri; ++k) Nij += ct[pos + k * qi + j];

                  if (Nij != 0) {
                      F[i](Nij);
                      for (int k = 0; k < ri; ++k) {
                          int Nijk = ct[pos + k * qi + j];
                          if (Nijk) F[i](Nijk, Nij);
                      }
                  }
              } // for j

              F[i].finalize(-1);
          } // for i
      } // apply

      template <typename score_functor>
      void apply(int xi, const set_type& pa, score_functor& F) const {
          std::vector<int> xi_vect{xi};
          std::vector<score_functor> F_vect{F};
          apply(xi_vect, pa, F_vect);
          F = F_vect[0];
      } // apply


      bool is_reorderable() { return true; }

      bool reorder(const std::vector<int>& norder) {
          std::vector<int> r_temp;
          std::vector<data_type> D_temp;

          r_temp.reserve(n_);
          D_temp.reserve(D_.size());

          for (const int xi : norder) {
              r_temp.push_back(r_[xi]);
              D_temp.insert(D_temp.end(), D_.begin() + xi * m_, D_.begin() + (xi + 1) * m_);
          }

          r_ = std::move(r_temp);
          D_ = std::move(D_temp);

          return true;
      } // reorder


  private:
      int n_;
      int m_;

      std::vector<int> r_;
      std::vector<data_type> D_;

      template <int M, typename Iter>
      friend CTCounter<M> create_CTCounter(int, int, Iter);
  }; // class CTCounter


  template <int N, typename Iter> CTCounter<N> create_CTCounter(int n, int m, Iter it) {
      CTCounter<N> p;

      p.n_ = n;
      p.m_ = m;
      p.r_.resize(n);

      p.D_.resize(n * m);
      std::copy_n(it, n * m, std::begin(p.D_));

      auto iter = std::begin(p.D_);

      for (int i = 0; i < n; ++i, iter += m) {
          auto res = std::minmax_element(iter, iter + m);
          p.r_[i] = (*res.second - *res.first) + 1;
      }

      return p;
  } // create_CTCounter

} // sabnatk

#endif // CT_COUNTER_HPP

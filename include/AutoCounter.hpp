/***
 *  $Id$
 **
 *  File: AutoCounter.hpp
 *  Created: Jan 25, 2022
 *
 *  Author: Pawel Bratek <pawel.bratek@pcz.pl>
 *  Copyright (c) 2022 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef AUTO_COUNTER_HPP
#define AUTO_COUNTER_HPP

#include "BVCounter.hpp"
#include "CTCounter.hpp"
#include "RadCounter.hpp"

#include "LRmodel.hpp"
#include "papiService.hpp"
#include "estimator.hpp"
#include <random>

#include "counters.hpp"


namespace sabnatk {

  template <int N, typename Data = uint8_t> class AutoCounter {
  public:
      using set_type = uint_type<N>;
      using data_type = Data;

      counter_type type() const { return Auto; }


      int n() const { return rad_.n(); }

      int m() const { return rad_.m(); }

      int r(int i) const { return rad_.r(i); }


      bool is_reorderable() { return true; }

      bool reorder(const std::vector<int>& norder) {
          rad_.reorder(norder);
          bv_.reorder(norder);
          ct_.reorder(norder);
          return true;
      } // reorder


      template <typename score_functor>
      void apply(const set_type& xi, const set_type& pa, std::vector<score_functor>& F) {

          // load pa variables of recent query
          as_vector(pa, recent_pa_variables_);

          // load xi variables of recent query
          // not included in model, used only in m_CT_possible__()
          as_vector(xi, recent_xi_variables_);

          // regression models ty = f(Npa, K)
          detail::LRmodel* model = nullptr;
          long long unsigned int ty = 0; // total number of cycles using PAPI

          int Npa = recent_pa_variables_.size();

          if (!all_models_trained_) {
              num_first_phase_choices_++;

              if (Npa <= N0_) {
                  if (Lmodels_to_train_size_ > 0) {
                      model = m_round_robin__(Lmodels_to_train_, Lmodels_to_train_size_, Lmodels_trained_, Lmodels_trained_size_, Npa);
                  } else {
                      model = m_select_best__(Lmodels_trained_, Lmodels_trained_size_, Npa);
                  }
              } else {
                  if (Rmodels_to_train_size_ > 0) {
                      model = m_round_robin__(Rmodels_to_train_, Rmodels_to_train_size_, Rmodels_trained_, Rmodels_trained_size_, Npa);
                  } else {
                      model = m_select_best__(Rmodels_trained_, Rmodels_trained_size_, Npa);
                  }
              }

              all_models_trained_ = Lmodels_to_train_size_ == 0 && Rmodels_to_train_size_ == 0;
          } else {
              if (Npa <= N0_) {
                  model = m_select_best__(Lmodels_trained_, Lmodels_trained_size_, Npa);
              } else {
                  model = m_select_best__(Rmodels_trained_, Rmodels_trained_size_, Npa);
              }
          }

          ty = m_run_query__(model, xi, pa, F);

          model->update(Npa, ty, K_);

      } // apply

  private:
      RadCounter<N> rad_;
      BVCounter<N> bv_;
      CTCounter<N> ct_;

      detail::LRmodel model_rad_;
      detail::LRmodel model_bv1_, model_bv2_;
      detail::LRmodel model_ct1_, model_ct2_;

      // two groups (L, R) of models based on N0
      detail::LRmodel* Lmodels_to_train_[3];
      detail::LRmodel* Rmodels_to_train_[3];
      int Lmodels_to_train_size_;
      int Rmodels_to_train_size_;

      detail::LRmodel* Lmodels_trained_[3];
      detail::LRmodel* Rmodels_trained_[3];
      int Lmodels_trained_size_;
      int Rmodels_trained_size_;

      bool all_models_trained_;

      // query statistics
      double K_;
      int N0_;

      int num_first_phase_choices_;

      int required_instances_; // the minimum number of data required to use the model during auto selection
      long long unsigned int CT_memory_limit_;

      PAPIService papi_;

      std::vector<int> recent_pa_variables_;
      std::vector<int> recent_xi_variables_;


      detail::LRmodel* m_round_robin__(detail::LRmodel** models_to_train, int& to_train_size,
                                       detail::LRmodel** models_trained, int& trained_size, int Npa) {
          int idx = num_first_phase_choices_ % to_train_size;

          if (((models_to_train[idx] == &model_ct2_) || (models_to_train[idx] == &model_ct1_)) && !m_CT_possible__()) {
              if (to_train_size > 1) idx = (num_first_phase_choices_ + 1) % to_train_size;
              else return m_select_best__(models_trained, trained_size, Npa);
          }

          detail::LRmodel* selected_model = models_to_train[idx];

          if (selected_model->n() >= required_instances_) {
              if (idx != (to_train_size - 1)) {
                  models_to_train[idx] = models_to_train[to_train_size - 1];
                  models_to_train[to_train_size - 1] = selected_model;
              }

              to_train_size--;

              models_trained[trained_size] = selected_model;
              trained_size++;
          }

          return selected_model;
      } // m_round_robin__

      detail::LRmodel* m_select_best__(detail::LRmodel** models, int models_size, int Npa) {
          std::vector<double> costs;
          costs.reserve(models_size);
          for (int i = 0; i < models_size; ++i) costs.push_back(models[i]->evaluate(Npa, K_));

          int idx = std::min_element(costs.begin(), costs.end()) - costs.begin();
          detail::LRmodel* best_model = models[idx];

          if (((best_model == &model_ct2_) || (best_model == &model_ct1_)) && !m_CT_possible__()) {
              costs[idx] = std::numeric_limits<double>::max();
              best_model = models[std::min_element(costs.begin(), costs.end()) - costs.begin()];
          }

          return best_model;
      } // m_select_best__

      bool m_CT_possible__() const {
          long long unsigned int vec_size = sizeof(int);

          // overflow possible so checking vector size after each xi and pa variable
          for (int i = 0; i < recent_xi_variables_.size(); ++i) {
              vec_size *= ct_.r(recent_xi_variables_[i]);
              if (vec_size > CT_memory_limit_) return false;
          }

          for (int i = 0; i < recent_pa_variables_.size(); ++i) {
              vec_size *= ct_.r(recent_pa_variables_[i]);
              if (vec_size > CT_memory_limit_) return false;
          }

          return true;
      } // m_CT_possible__

      template <typename score_functor>
      long long unsigned int m_run_query__(detail::LRmodel* model, const set_type& xi, const set_type& pa, std::vector<score_functor>& F) {
          PAPI_accum(papi_.eventset, &papi_.count);
          papi_.count = 0;

          if (model->counter() == Rad) rad_.apply(xi, pa, F);
          else if (model->counter() == BV) bv_.apply(xi, pa, F);
          else ct_.apply(xi, pa, F);

          PAPI_accum(papi_.eventset, &papi_.count);

          return papi_.count;
      } // m_run_query__

      template <int M, typename Iter>
      friend AutoCounter<M> create_AutoCounter(int, int, Iter, int, int, int);
  }; // class AutoCounter


  template <int N, typename Iter> AutoCounter<N> create_AutoCounter(int n, int m, Iter it, int burn_in = -1, int CT_memory_limit = 32, int required_instances = 3) {
      AutoCounter<N> p;

      p.rad_ = create_RadCounter<N>(n, m, it);
      p.bv_ = create_BVCounter<N>(n, m, it);
      p.ct_ = create_CTCounter<N>(n, m, it);

      p.CT_memory_limit_ = static_cast<long long unsigned int>(CT_memory_limit) * 1048576; // MiB to B

      p.required_instances_ = required_instances;

      p.model_bv1_ = detail::LRmodel(BV, false);
      p.model_bv2_ = detail::LRmodel(BV, true);
      p.model_ct1_ = detail::LRmodel(CT, true);
      p.model_ct2_ = detail::LRmodel(CT, false);
      p.model_rad_ = detail::LRmodel(Rad, true);

      p.Lmodels_to_train_[0] = &p.model_rad_;
      p.Lmodels_to_train_[1] = &p.model_bv1_;
      p.Lmodels_to_train_[2] = &p.model_ct1_;
      p.Rmodels_to_train_[0] = &p.model_rad_;
      p.Rmodels_to_train_[1] = &p.model_bv2_;
      p.Rmodels_to_train_[2] = &p.model_ct2_;

      p.Lmodels_to_train_size_ = 3;
      p.Rmodels_to_train_size_ = 3;

      for (int i = 0; i < 3; ++i) p.Lmodels_trained_[i] = nullptr;
      for (int i = 0; i < 3; ++i) p.Rmodels_trained_[i] = nullptr;

      p.Lmodels_trained_size_ = 0;
      p.Rmodels_trained_size_ = 0;

      p.all_models_trained_ = false;

      p.K_ = 0.0;

      if (n > 0) {
          for (int i = 0; i < n; ++i) p.K_ += p.ct_.r(i);
          p.K_ /= n;
      }

      p.N0_ = static_cast<int>(std::log(p.m()) / std::log(p.K_));

      p.num_first_phase_choices_ = 0;

      if (burn_in > 0) {
          std::random_device rd;
          std::default_random_engine eng(rd());
          std::uniform_int_distribution<int> size(0, n-1);
          std::vector<estimator> F(1);

          std::vector<int> v;
          for (int i = 0; i < n; ++i) v.push_back(i);

          for (int i = 0; i < burn_in; ++i) {
            std::shuffle(v.begin(), v.end(), eng);

            // first elements of v become the parents, and the last element of v becomes xi
            std::vector<int> pa (v.begin(), v.begin() + size(eng));
            std::vector<int> xi {v[n-1]};

            p.apply(as_set<uint_type<N>>(xi.begin(), xi.end()), as_set<uint_type<N>>(pa.begin(), pa.end()), F);
          }
      }

      return p;
  } // create_AutoCounter

} // namespace sabnatk

#endif // AUTO_COUNTER_HPP

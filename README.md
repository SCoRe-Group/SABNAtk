# SABNAtk: Fast Counting in Machine Learning Applications

**Authors:**
Subhadeep Karan <skaran@buffalo.edu>,
Pawel Bratek <pawel.bratek@pcz.pl>,
Matthew Eichhorn <maeichho@buffalo.edu>,
Blake Hurlburt <blakehur@buffalo.edu>,
Grant Iraci <grantira@buffalo.edu>,
Mohammad Umair <m39@buffalo.edu>,
Jaroslaw Zola <jaroslaw.zola@hush.com>

## About

SABNAtk is a small C++17 library, together with Python 3 bindings, designed to efficiently execute counting queries over categorical data. Counting queries are common to Machine Learning applications, for example, they show up in Probabilistic Graphical Models, regression analysis, etc. In practical applications, SABNAtk [significantly outperforms](https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks) typical approaches based on, e.g., hash tables or ADtrees. Currently, SABNAtk is powering [SABNA](https://gitlab.com/SCoRe-Group/SABNA-Release), our Bayesian networks learning engine. We are constantly working to improve performance (including support for GPGPUs), so stay tuned!

This project has been supported by [NSF](https://www.nsf.gov/) under the award [OAC-1845840](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1845840).


## User Guide

### Quick Install

* If you are impatient, run `./build.sh` in the project's root directory. If the `Boost.Python` library is installed in a non-standard location, you may want to edit `build.sh` to specify `BOOST_ROOT`. If you want to use Python different than Python 3.8, you will have to edit `CMakeLists.txt`.


### Install

1. Make sure you have a recent C++ compiler with support for C++17 and SIMD vectorization.
2. Make sure that you have `python3.8` and `Boost.Python >= 1.67` library installed, they are required if you want Python bindings.
3. Make sure that you have `cmake >= 3.1` available.
4. Run `./build.sh` in the project root directory (see also **Quick Install**).
5. If the run is successful, you should see `sabnatk.so` object, which implements Python bindings.
6. If you do not need Python bindings you can use `include/` folder only, since from the C++ perspective `SABNAtk` is a headers-only library.
7. To integrate `SABNAtk` with your C++ project, you can make the entire `SABNAtk` source tree a subfolder in your source code tree, and then use `ADD_SUBDIRECTORY` directive in your `CMakeLists.txt`. Alternatively, you can copy `include/` directory and pass it to `-I` switch when compiling.
8. To integrate `SABNAtk` with your Python project treat `sabnatk.so` as a Python module (see documentation below).


### C++ API

If you have questions not answered by this documentation, please do not hesitate to contact Jaric Zola <jaroslaw.zola@hush.com>.

To use `SABNAtk` we will follow this simple workflow (we assume that you are somewhat familiar with the original UAI 2018 paper on `SABNAtk`). First, we will create a counts enumeration object (counter), either `BVCounter` or `RadCounter`, then we will implement a functor to aggregate our counts, finally we will combine the two and start applying queries.

1. The example below shows how to create a counts enumeration object. We recommend that you use `RadCounter` as most of the time it offers best performance:

   ```c++
   #include <sabnatk.hpp>

   // 3 variables (say X0, X1, X2), 8 observations
   std::vector<uint8_t> Data{ 0, 1, 1, 0, 1, 1, 0, 0, \
                              0, 0, 2, 0, 1, 2, 0, 1, \
                              1, 1, 1, 0, 1, 1, 1, 0 };
   const int N = 1;

   int n = 3;
   int m = 8;

   auto rad = sabnatk::create_counter<N, sabnatk::Rad>(n, m, std::begin(Data));
   ```

   In this example, `Data` is a sequence storing our data (any sequence supporting forward iteration should do). The data must be stored row-wise (i.e., one variable per row, rows placed continuously in the memory). `n` is the number of variables (i.e., the number of rows in the data), and `m` is the number of columns (i.e., the number of instances/observations in the data). Parameter `N` tells `SABNAtk` how many words it should be using to manage the data. Specifically, we require that `n < 64 * N`. Note that once `rad` object is created, `Data` is no longer needed.

2. To process counts generated for a specific counting query we have to implement a functor that we will pass to our counts enumeration object. Consider a counting query with variables $`X_i`$ and $`Pa(X_i)`$, where $`Pa(X_i)`$ is a set of variables. `SABNAtk` extracts two types of counts: $`N_{ij}`$ and $`N_{ijk}`$. $`N_{ij}`$ is the count of data instances that support state $`j`$ of variables $`Pa(X_i)`$. For example, for `Data` and variables $`Pa(X_0)=\{X_1,X_2\}`$ we could assign state $`j=0`$ to $`(X_1=0,X_2=0)`$, $`j=1`$ to $`(X_1=0,X_2=1)`$ and so on, with the total of $`q_i=3 \times 2 = 6`$ states. Then, $`N_{00}`$ would be $`1`$, and $`N_{01}`$ would be $`3`$, with `Data` supporting $`5`$ different states of $`Pa(X_0)`$. $`N_{ijk}`$ on the other hand is the count of instances such that variables $`Pa(X_i)`$ are in state $`j`$, and variable $`X_i`$ is in the state $`k`$. For example, if we assign $`k=0`$ to $`X_0=0`$ and $`k=1`$ to $`X_0=1`$, then $`N_{010}`$ will be $`2`$. `SABNAtk` does not care what you do with the counts $`N_{ij}`$ and $`N_{ijk}`$, but it offers extremely fast mechanism to give you these counts. Consider a toy example of the log-likelihood score defined as: $`\mathcal{L}(X_i|Pa(X_i))=\sum_{j}\sum_{k}N_{ijk}\log\left(\frac{N_{ijk}}{N_{ij}}\right)`$. We can implement $`\mathcal{L}`$ as the following functor:

   ```c++
   class L {
   public:
     using score_type = double

     // Count enumeration object will call this method
     // when starting counting
     void init(int ri, int qi) {
       score_ = 0.0; // we ignore ri and qi
     }

     // Count enumeration object will call this method
     // to finalize counting, and qi will be the actual number
     // of distinct states of Pa(Xi) found in the data
     // or -1 if such information cannot be provided efficiently
     void finalize(int qi) { }

     // Count enumeration object will call this method
     // when count for new state j of Pa(Xi) is obtained
     void operator()(int Nij) { }

     // Count enumeration object will call this method
     // when count for new state k of Xi is obtained
     // Nij is count for the corresponding state j of Pa(Xi)
     void operator()(int Nijk, int Nij) {
       score_ = Nijk * log2(Nijk / Nij);
     }

     // Extra method to access the final result
     // (not used by count enumeration object)
     score_type score() const { return score_; }

   private:
     score_type score_;
   };
   ```

   `SABNAtk` requires that user specified functor supports the following methods:
   * `void init(int ri, int qi);` This method is called at the beginning of counting. Count enumeration object passes `ri`, the number of states of query variable $`X_i`$, and `qi`, the expected number of states of query variables $`Pa(X_i)`$.
   * `void finalize(int qi);` This methods is called at the end of counting. Count enumeration object passes `qi`, which is the observed (in the data) number of states of variables $`Pa(X_i)`$. If this number cannot be obtained efficiently, which may be the case for some counting strategies, the count enumeration object will return `-1`.
   * `void operator()(int Nij);` This operator is called each time the total count of instances supporting state $`j`$ of variables $`Pa(Xi)`$ is obtained.
   * `void operator()(int Nijk, Nij);` This operator is called each time new count $`N_{ijk}`$ is obtained.


### Python API

The fastest way you learn Python API is to look at the code in `examples/mdl.py`, `examples/bdeu.py` and `examples/bnscore.py` files. This section is still under construction! If you have immediate questions, please do not hesitate to contact Jaric Zola <jaroslaw.zola@hush.com>.


## References

To cite SABNAtk, refer to this repository and our 2018 UAI paper:

* S. Karan, M. Eichhorn, B. Hurlburt, G. Iraci, J. Zola, _Fast Counting in Machine Learning Applications_, In Proc. Uncertainty in Artificial Intelligence (UAI), 2018. <https://arxiv.org/abs/1804.04640>.

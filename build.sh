#!/bin/bash

clear
DIR=`pwd`

mkdir -p build/
rm -rf build/*
cd build/

# Pick one of the cmake calls depending on your Boost.Python configuration
# Use -DBOOST_STATIC=True to use static version of Boost libraries
cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR

#cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR \
#          -DBOOST_ROOT=/usr/local

#cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR \
#          -DBOOST_INCLUDEDIR=$BOOST_INC_DIR \
#          -DBOOST_LIBRARYDIR=$BOOST_LIB_DIR

make -j4 install

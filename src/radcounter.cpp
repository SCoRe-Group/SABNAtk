/***
 *  $Id$
 **
 *  File: radcounter.cpp
 *  Created: Nov 06, 2017
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <cstring>
#include <vector>

#include <AIC.hpp>
#include <BDeu.hpp>
#include <MDL.hpp>
#include <RadCounter.hpp>
#include <bit_util.hpp>

#include <boost/python.hpp>


namespace bpy = boost::python;


namespace radc {

  template <int N> class radcounter_base {
  public:
      using data_type = uint8_t;
      using set_type = typename sabnatk::RadCounter<N, data_type>::set_type;

      void init(int n, int m, const bpy::list& L) {
          p_rc_ = sabnatk::create_RadCounter<N, data_type>(n, m, bpy::stl_input_iterator<data_type>(L));
      } // init

      void reorder(const bpy::list& L) {
          auto n = bpy::len(L);

          std::vector<int> v(n);
          for (int i = 0; i < n; ++i) v[i] = bpy::extract<int>(L[i]);

          p_rc_.reorder(v);
      } // reorder

      int r(int xi) const { return p_rc_.r(xi); }

  protected:
      template <typename Fun>
      bpy::list m_score__(const bpy::object& xi, const bpy::object& pa, std::vector<Fun>& F, const Fun& f) {
          set_type xi_set = set_empty<set_type>();
          set_type pa_set = set_empty<set_type>();

          const std::string& xi_buf = bpy::extract<std::string>(xi);
          std::memcpy(&xi_set, xi_buf.c_str(), xi_buf.size());

          const std::string& pa_buf = bpy::extract<std::string>(pa);
          std::memcpy(&pa_set, pa_buf.c_str(), pa_buf.size());

          int n_xi = set_size(xi_set);
          bpy::list res;

          F.resize(n_xi);
          for (auto& fun : F) fun = f;

          p_rc_.apply(xi_set, pa_set, F);

          for (int i = 0; i < n_xi; ++i) {
              auto s = F[i].score();
              res.append(bpy::make_tuple(std::get<0>(s), std::get<1>(s)));
          }

          return res;
      } // m_score__

      sabnatk::RadCounter<N> p_rc_;

  }; // class radcounter_base


  template <int N> class AICtk : public radcounter_base<N> {
  public:
      bpy::list score(const bpy::object& xi, const bpy::object& pa) {
          return this->m_score__(xi, pa, F_, AIC());
      } // score

  private:
      std::vector<AIC> F_;

  }; // class AICtk

  template <int N> class BDeutk : public radcounter_base<N> {
  public:
      double alpha = 1.0;

      bpy::list score(const bpy::object& xi, const bpy::object& pa) {
          return this->m_score__(xi, pa, F_, BDeu(alpha));
      } // score

  private:
      std::vector<BDeu> F_;

  }; // class BDeutk

  template <int N> class MDLtk : public radcounter_base<N> {
  public:
      bpy::list score(const bpy::object& xi, const bpy::object& pa) {
          return this->m_score__(xi, pa, F_, MDL(this->p_rc_.m()));
      } // score

  private:
      std::vector<MDL> F_;

  }; // class MDLtk

  template <int N> class Query : public radcounter_base<N> {
  public:
      using data_type = uint8_t;
      using set_type = typename radcounter_base<N>::set_type;

      bpy::list run(const bpy::list& xi, const bpy::list& pa,
                    const bpy::list& xi_st, const bpy::list& pa_st) {
          set_type xi_set = set_empty<set_type>();
          auto n_xi = bpy::len(xi);
          for (int i = 0; i < n_xi; ++i) xi_set = set_add(xi_set, bpy::extract<int>(xi[i]));

          set_type pa_set = set_empty<set_type>();
          auto n_pa = bpy::len(pa);
          for (int i = 0; i < n_pa; ++i) pa_set = set_add(pa_set, bpy::extract<int>(pa[i]));

          state_xi_.resize(n_xi);
          state_pa_.resize(n_pa);

          for (int i = 0; i < n_xi; ++i) state_xi_[i] = bpy::extract<int>(xi_st[i]);
          for (int i = 0; i < n_pa; ++i) state_pa_[i] = bpy::extract<int>(pa_st[i]);

          F_.resize(n_xi);

          this->p_rc_.apply(xi_set, pa_set, state_xi_, state_pa_, F_);

          bpy::list res;
          for (int i = 0; i < n_xi; ++i) res.append(F_[i].score());

          return res;
      } // run

  private:
      struct call__ {
          void init(int, int) { nijk = 0; }
          void finalize(int) { }
          void operator()(int) { }
          void operator()(int Nijk, int) { nijk = Nijk; }
          int score() const { return nijk; }
          int nijk = 0;
      }; // struct call__

      std::vector<data_type> state_xi_;
      std::vector<data_type> state_pa_;
      std::vector<call__> F_;

  }; // class Query

} // namespace radc


void export_RadCounter() {
    bpy::object rc_module(bpy::handle<>(bpy::borrowed(PyImport_AddModule("sabnatk.RadCounter"))));
    bpy::scope().attr("RadCounter") = rc_module;

    bpy::scope rc_scope = rc_module;

    bpy::class_<radc::AICtk<1>>("AIC64")
        .def("init", &radc::radcounter_base<1>::init)
        .def("reorder", &radc::radcounter_base<1>::reorder)
        .def("score", &radc::AICtk<1>::score)
        .def("r", &radc::radcounter_base<1>::r);

    bpy::class_<radc::AICtk<2>>("AIC128")
        .def("init", &radc::radcounter_base<2>::init)
        .def("reorder", &radc::radcounter_base<2>::reorder)
        .def("score", &radc::AICtk<2>::score)
        .def("r", &radc::radcounter_base<2>::r);

    bpy::class_<radc::AICtk<4>>("AIC256")
        .def("init", &radc::radcounter_base<4>::init)
        .def("reorder", &radc::radcounter_base<4>::reorder)
        .def("score", &radc::AICtk<4>::score)
        .def("r", &radc::radcounter_base<4>::r);

    bpy::class_<radc::BDeutk<1>>("BDeu64")
        .def_readwrite("alpha", &radc::BDeutk<1>::alpha)
        .def("init", &radc::radcounter_base<1>::init)
        .def("reorder", &radc::radcounter_base<1>::reorder)
        .def("score", &radc::BDeutk<1>::score)
        .def("r", &radc::radcounter_base<1>::r);

    bpy::class_<radc::BDeutk<2>>("BDeu128")
        .def_readwrite("alpha", &radc::BDeutk<2>::alpha)
        .def("init", &radc::radcounter_base<2>::init)
        .def("reorder", &radc::radcounter_base<2>::reorder)
        .def("score", &radc::BDeutk<2>::score)
        .def("r", &radc::radcounter_base<2>::r);

    bpy::class_<radc::BDeutk<4>>("BDeu256")
        .def_readwrite("alpha", &radc::BDeutk<4>::alpha)
        .def("init", &radc::radcounter_base<4>::init)
        .def("reorder", &radc::radcounter_base<4>::reorder)
        .def("score", &radc::BDeutk<4>::score)
        .def("r", &radc::radcounter_base<4>::r);

    bpy::class_<radc::MDLtk<1>>("MDL64")
        .def("init", &radc::radcounter_base<1>::init)
        .def("reorder", &radc::radcounter_base<1>::reorder)
        .def("score", &radc::MDLtk<1>::score)
        .def("r", &radc::radcounter_base<1>::r);

    bpy::class_<radc::MDLtk<2>>("MDL128")
        .def("init", &radc::radcounter_base<2>::init)
        .def("reorder", &radc::radcounter_base<2>::reorder)
        .def("score", &radc::MDLtk<2>::score)
        .def("r", &radc::radcounter_base<2>::r);

    bpy::class_<radc::MDLtk<4>>("MDL256")
        .def("init", &radc::radcounter_base<4>::init)
        .def("reorder", &radc::radcounter_base<4>::reorder)
        .def("score", &radc::MDLtk<4>::score)
        .def("r", &radc::radcounter_base<4>::r);

    bpy::class_<radc::Query<1>>("Query64")
        .def("init", &radc::radcounter_base<1>::init)
        .def("reorder", &radc::radcounter_base<1>::reorder)
        .def("run", &radc::Query<1>::run);

    bpy::class_<radc::Query<2>>("Query128")
        .def("init", &radc::radcounter_base<2>::init)
        .def("reorder", &radc::radcounter_base<2>::reorder)
        .def("run", &radc::Query<2>::run);

    bpy::class_<radc::Query<4>>("Query256")
        .def("init", &radc::radcounter_base<4>::init)
        .def("reorder", &radc::radcounter_base<4>::reorder)
        .def("run", &radc::Query<4>::run);

} // export_RadCounter
